/*
 * transparent_mode.hpp
 *
 *  Created on: Sep 18, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_MODES_TRANSPARENT_MODE_HPP_
#define INC_MODES_TRANSPARENT_MODE_HPP_

#include "hal.hpp"
#include "interfaces/mode.hpp"

class TransparentMode : public Mode
{
public:
	void resetMode();
	void updateMode();
};

#endif /* INC_MODES_TRANSPARENT_MODE_HPP_ */
