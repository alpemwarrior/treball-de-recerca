/*
 * log_manager.hpp
 *
 *  Created on: 2 d’ag. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_LOG_MANAGER_HPP_
#define INC_LOG_MANAGER_HPP_

#include "fatfs.h"

class LogManager
{
public:
	void init(FIL* file_handle);
	void print(const char* message);
private:
	FIL* file_handle;
};

#endif /* INC_LOG_MANAGER_HPP_ */
