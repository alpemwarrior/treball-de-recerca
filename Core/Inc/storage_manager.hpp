/*
 * storage_manager.hpp
 *
 *  Created on: Jul 30, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_STORAGE_MANAGER_HPP_
#define INC_STORAGE_MANAGER_HPP_

#include <stdint.h>
#include "fatfs.h"

#include "interfaces/job.hpp"

#include "config_manager.hpp"
#include "log_manager.hpp"
#include "blackbox.hpp"

#define CONFIG_FILE   "config.ini"
#define LOG_FILE 	  "log.txt"
#define BLACKBOX_FILE "blackbox.jsl"

class StorageManager : public Job
{
public:
	void init(job_period freq);
	void save();
	void close();

	void jobLoop();

	LogManager*    getLogManager();
	ConfigManager* getConfigManager();

private:
	void open();

	FATFS FatFs;
	FRESULT fres;

	ConfigManager config_manager;
	LogManager    log_manager;
	Blackbox	  blackbox;


	FIL config_file;
	FIL log_file;
	FIL blackbox_file;
};



#endif /* INC_STORAGE_MANAGER_HPP_ */
