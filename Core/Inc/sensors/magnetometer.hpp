/*
 * magnetometer.hpp
 *
 * Describes an interface for all objects implementing a magnetometer sensor.
 *
 *  Created on: Jul 25, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_MAGNETOMETER_HPP_
#define INC_MAGNETOMETER_HPP_

class Magnetometer
{
public:
	virtual void magCalibrate()  = 0;

	//Readings are stored in signed integers and in uT
	float mag_x;
	float mag_y;
	float mag_z;
};

#endif /* INC_MAGNETOMETER_HPP_ */
