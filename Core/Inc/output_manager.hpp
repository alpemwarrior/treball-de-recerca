/*
 * output_manager.hpp
 *
 *  Created on: Sep 16, 2021
 *      Author: alpemwarrior
 */

#ifndef INC_OUTPUT_MANAGER_HPP_
#define INC_OUTPUT_MANAGER_HPP_

#include <stdint.h>

#include "config/hal_conf.h"
#include "interfaces/job.hpp"
#include "interfaces/servo.hpp"

#define OUTPUT_MANAGER_TRIM_YAW_KEY   "trim_y"
#define OUTPUT_MANAGER_TRIM_ROLL_KEY  "trim_r"
#define OUTPUT_MANAGER_TRIM_PITCH_KEY "trim_p"

class OutputManager : public Job
{
public:
	OutputManager();
	void init(Servo** servo_list, uint8_t size);
	void jobLoop();

	void setFromUnitAndClamped(float value, uint8_t index);

	void set(uint8_t index, int16_t value);
	uint16_t get(uint8_t index);

private:

	uint16_t out_values[HAL_SERVO_CHANNELS];
	int32_t trim_yaw, trim_pitch, trim_roll;

	uint8_t size;
	Servo** servo_list;
};



#endif /* INC_OUTPUT_MANAGER_HPP_ */
