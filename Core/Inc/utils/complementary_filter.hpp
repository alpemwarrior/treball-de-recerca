/*
 * complementary_filter.hpp
 *
 *  Created on: Sep 7, 2021
 *      Author: alpemwarrior
 */

#ifndef SRC_COMPLEMENTARY_FILTER_HPP_
#define SRC_COMPLEMENTARY_FILTER_HPP_

#include "utils/quaternion.hpp"
#include "gpio.h"

#define ACCEL_ORIENTATION_MIN_GRAVITY 9.5
#define ACCEL_ORIENTATION_MAX_GRAVITY 10.2

class ComplementaryFilter
{
public:
	void init(float period, float tau);

	void loop(float* gyro_x,  float* gyro_y,  float* gyro_z,
			  float* accel_x, float* accel_y, float* accel_z,
			  float* mag_x,   float* mag_y,   float* mag_z );
	unsigned long last_read;
		float alpha;
		bool initial;

		float var1;
		float var2;
		float var3;

		Quaternion orientation;
private:
	bool setOrientationFromAccelandMag(float* accel_x, float* accel_y, float* accel_z,
									   float* mag_x,   float* mag_y,   float* mag_z);
};


#endif /* SRC_COMPLEMENTARY_FILTER_HPP_ */
