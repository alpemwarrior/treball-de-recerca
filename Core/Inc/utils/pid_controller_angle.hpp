/*
 * pid_controller.hpp
 *
 *  Created on: 4 d’oct. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_UTILS_PID_CONTROLLER_ANGLE_HPP_
#define INC_UTILS_PID_CONTROLLER_ANGLE_HPP_

#include "interfaces/job.hpp"

#define PID_CONTROLLER_ANGLE_XP_KEY "apid_x_p"
#define PID_CONTROLLER_ANGLE_XI_KEY "apid_x_i"
#define PID_CONTROLLER_ANGLE_XD_KEY "apid_x_d"

#define PID_CONTROLLER_ANGLE_YP_KEY "apid_y_p"
#define PID_CONTROLLER_ANGLE_YI_KEY "apid_y_i"
#define PID_CONTROLLER_ANGLE_YD_KEY "apid_y_d"

#define PID_CONTROLLER_ANGLE_ZP_KEY "apid_z_p"
#define PID_CONTROLLER_ANGLE_ZI_KEY "apid_z_i"
#define PID_CONTROLLER_ANGLE_ZD_KEY "apid_z_d"

class ControllerPIDAngle
{
public:
	ControllerPIDAngle();

	void setParameters(float proportional, float integral, float derivative);
 	void setSetpoint(float setpoint);

 	void reset();

	float loop(float measurement);

private:
	float setpoint;

	float p;
	float i;
	float d;

	float integral_sum;
	float last_error;

	float last_time;
};


#endif /* INC_UTILS_PID_CONTROLLER_ANGLE_HPP_ */
