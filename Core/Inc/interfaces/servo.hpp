/*
 * servo.hpp
 *
 *  Created on: 10 de jul. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_SERVO_HPP_
#define INC_SERVO_HPP_

#include <stdint.h>

class Servo
{
public:
	virtual void update(uint16_t value) = 0;
};



#endif /* INC_SERVO_HPP_ */
