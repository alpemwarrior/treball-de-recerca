/*
 * rx_ibus.hpp
 *
 *  Created on: 10 de jul. 2021
 *      Author: alpemwarrior
 */

#ifndef INC_RX_IBUS_HPP_
#define INC_RX_IBUS_HPP_

#include "config/hal_conf.h"
#include "interfaces/serial.hpp"
#include "interfaces/rx.hpp"
#include "serial_manager.hpp"
#include "task_manager.hpp"

#define BUFFER_SIZE           0x21
#define SERVO_FRAME_SIZE      0x20

//The local buffer should be smaller or equal to the public buffer
#define RX_IBUS_CHANNELS      14

#if RX_IBUS_CHANNELS > HAL_RX_CHANNELS
#error
#endif

class RxIBUS : public Rx, public Serial, public Job
{
public:
	RxIBUS(int serial_index);
	void receive(uint8_t new_data);
	void update();
	void jobLoop();

#if HAL_RX_IBUS_DEBUG_FRAMERATE
	uint32_t frametime;
	uint32_t last_frame;
	uint32_t framerate;
	void updateFramerate();
#endif

private:
	bool checkFrame(uint8_t data_size, uint8_t first);
	void saveChannelData(uint8_t head);

	//Temporary buffer to store data values before copying them to public registers.
	uint16_t channels[RX_IBUS_CHANNELS];

	//Circular buffer to store received serial data
	uint8_t buffer[BUFFER_SIZE];

	//Data pointer for the circular buffer
	uint8_t  index;

	//Has the public buffer already been updated
	bool changed;
};

#endif /* INC_RX_IBUS_HPP_ */
