/*
 * log_manager_wrapper.cpp
 *
 *  Created on: Oct 3, 2021
 *      Author: alpemwarrior
 */

#include "log_manager_wrapper.hpp"

void writeLog(const char* string)
{
	if (HAL::getInstance().getStorageManager()->getLogManager())
	{
		HAL::getInstance().getStorageManager()->getLogManager()->print(string);
	}
	return;
}


