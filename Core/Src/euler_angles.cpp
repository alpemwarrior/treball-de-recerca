/*
 * euler_angles.cpp
 *
 *  Created on: Oct 11, 2021
 *      Author: alpemwarrior
 */

#include "utils/euler_angles.hpp"

#define M_PI 3.14159265358979323846

EulerAngles eulerAnglesFromQuaternion(Quaternion q)
{
	EulerAngles ea = {
		atan2(2 * (q.a * q.b + q.c * q.d), 1 - 2 * (pow(q.b, 2.0) + pow(q.c, 2.0))),
		asin(2 * (q.a * q.c - q.d * q.b)),
		atan2(2 * (q.a * q.d + q.b * q.c), 1 - 2 * (pow(q.c, 2.0) + pow(q.d, 2.0)))
	};

	if (ea.y > (M_PI))
	{
		return ea;
	}
	return ea;
}


