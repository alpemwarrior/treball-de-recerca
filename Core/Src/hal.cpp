/*
 * hal.cpp
 *
 *  Created on: 10 de jul. 2021
 *      Author: alpemwarrior
 */

/* STM32 includes   */
#include <i2c.h>
#include <tim.h>

/* Private includes */
#include "hal.hpp"
#include "debug_utils.hpp"
#include "serial_manager.hpp"
#include "drivers/icm_20948_i2c.hpp"
#include "drivers/rx_ibus.hpp"
#include "drivers/servo_pwm.hpp"

HAL& HAL::getInstance()
{
	static HAL hal;
	return hal;
}

void HAL::init()
{
	task_manager.init();

#if HAL_USE_STORAGE_MANAGER
	storage_manager.init(JOB_FREQ_5S);
#endif

	static ICM_20948_I2C icm_20948_i2c = ICM_20948_I2C(HAL_IMU_FREQ, &hi2c1, false);

	accel = &icm_20948_i2c;
	gyro  = &icm_20948_i2c;
	mag   = &icm_20948_i2c;

	static RxIBUS rx_ibus = RxIBUS(0);

	receiver = &rx_ibus;

	static Servo *servos[HAL_SERVO_CHANNELS];

	static ServoPWM servo1 = ServoPWM(&htim1,  TIM_CHANNEL_1, false);
	static ServoPWM servo2 = ServoPWM(&htim1,  TIM_CHANNEL_3, true);
	static ServoPWM servo3 = ServoPWM(&htim1,  TIM_CHANNEL_4, false);
	static ServoPWM servo4 = ServoPWM(&htim16, TIM_CHANNEL_1, false);

	servos[0] = &servo2;
	servos[1] = &servo1;
	servos[2] = &servo3;
	servos[3] = &servo4;

	output_manager.init((Servo**)servos, sizeof(servos) / sizeof(servos[0]));

	state_tracker.init(HAL_STATE_TRACKER_FREQ);

	motion_processor.init(HAL_MOTION_PROCESSOR_FREQ, accel, gyro, mag);
}

Rx*	HAL::getReceiver()
{
	return receiver;
}

Accelerometer* HAL::getAccelerometer()
{
	return accel;
}

Gyroscope* HAL::getGyroscope()
{
	return gyro;
}

Magnetometer* HAL::getMagnetometer()
{
	return mag;
}

TaskManager* HAL::getTaskManager()
{
	return &task_manager;
}

MotionProcessor* HAL::getMotionProcessor()
{
	return &motion_processor;
}

StorageManager* HAL::getStorageManager()
{
#if HAL_USE_STORAGE_MANAGER
	return &storage_manager;
#else
	return nullptr;
#endif
}

OutputManager* HAL::getOutputManager()
{
	return &output_manager;
}

void HAL::softFaultHandler()
{
	//When an error happens, this handler should be called to reset the plane to a safe state
}

