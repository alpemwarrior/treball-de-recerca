/*
 * config_manager.cpp
 *
 *  Created on: Jul 30, 2021
 *      Author: alpemwarrior
 */

#include "config/hal_conf.h"
#include "storage_manager.hpp"

ConfigManager* StorageManager::getConfigManager()
{
	return &config_manager;
}

LogManager* StorageManager::getLogManager()
{
	return &log_manager;
}

void StorageManager::init(job_period freq)
{
	fres = f_mount(&FatFs, "", 1);
	HAL_Delay(1000);
	if (fres != FR_OK) { return; }
	open();
	config_manager.init(&config_file);
	log_manager.init(&log_file);
	blackbox.init(&blackbox_file, HAL_BLACKBOX_FREQUENCY);
	jobRegister(freq);
	return;
}

void StorageManager::save()
{
	close();
	open();
}

void StorageManager::jobLoop()
{
	f_sync(&blackbox_file);
	f_sync(&log_file);
}

void StorageManager::open()
{
	fres = f_open(&config_file,   CONFIG_FILE,   FA_READ                    );
	fres = f_open(&log_file,      LOG_FILE,      FA_CREATE_ALWAYS | FA_WRITE);
	fres = f_open(&blackbox_file, BLACKBOX_FILE, FA_CREATE_ALWAYS | FA_WRITE);
	return;
}

void StorageManager::close()
{
	f_close(&config_file);
	f_close(&log_file);
	f_close(&blackbox_file);
	return;
}
