/*
 * config_manager.cpp
 *
 *  Created on: Aug 1, 2021
 *      Author: alpemwarrior
 */

#include "config_manager.hpp"
#include "fatfs.h"
#include "dependencies/ini.h"
#include <string.h>
#include <stdlib.h>
#include <cmath>
#include <climits>

void ConfigManager::init(FIL* file_handle)
{
	this->file_handle = file_handle;
}

int ConfigManager::INIHandler(void* user, const char* section, const char* name, const char* value, int lineno)
{
	value_pair* match = (value_pair*)user;
	if (!strcmp(match->key, name))
	{
		strncpy(match->value, value, sizeof(match->value) - 1);
		match->lineno = lineno;
	}
	return 1;
}

config_return_t ConfigManager::getValue(const char* key, float* value)
{
	value_pair match = {key, "", -1};
	if (ini_parse_fatfs(file_handle, &ConfigManager::INIHandler, &match) < 0)
	{
		return CONFIG_MISC_ERROR;
	}
	if (match.lineno < 0)
	{
		return CONFIG_NO_FIELD;
	}
	char * endptr;
	*value = strtod(match.value, &endptr);
	if ((*endptr != '\0') or abs(*value) == HUGE_VAL)
	{
		return CONFIG_FORMAT_ERROR;
	}
	return CONFIG_OK;
}

config_return_t ConfigManager::getValue(const char* key, int32_t* value)
{
	value_pair match = {key, "", -1};
	if (ini_parse_fatfs(file_handle, &ConfigManager::INIHandler, &match) < 0)
	{
		return CONFIG_MISC_ERROR;
	}
	if (match.lineno < 0)
	{
		return CONFIG_NO_FIELD;
	}
	char * endptr;
	*value = strtol(match.value, &endptr, 10);
	if ((*endptr != '\0') or *value == LONG_MAX or *value == LONG_MIN)
	{
		return CONFIG_FORMAT_ERROR;
	}
	return CONFIG_OK;
}

config_return_t ConfigManager::getValue(const char* key, char* value, uint8_t size)
{
	value_pair match = {key, "", -1};
	if (ini_parse_fatfs(file_handle, &ConfigManager::INIHandler, &match) < 0)
	{
		return CONFIG_MISC_ERROR;
	}
	if (match.lineno < 0)
	{
		return CONFIG_NO_FIELD;
	}
	strncpy(value, match.value, size);
	return CONFIG_OK;
}

config_return_t ConfigManager::findValue(const char * key, unsigned char * lineno)
{
	value_pair match = {key, "", -1};
	if (ini_parse_fatfs(file_handle, &ConfigManager::INIHandler, &match) < 0)
	{
		return CONFIG_MISC_ERROR;
	}
	if (match.lineno < 0)
	{
		return CONFIG_NO_FIELD;
	}
	*lineno = match.lineno;
	return CONFIG_OK;
}
