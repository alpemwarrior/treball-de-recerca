/*
 * icm_20948_i2c.cpp
 *
 *  Created on: 26 de jul. 2021
 *      Author: alpemwarrior
 */

#include "drivers/icm_20948_i2c.hpp"
#include "log_manager_wrapper.hpp"
#include "hal.hpp"
#include <math.h>

# define M_PI           3.14159265358979323846

ICM_20948_I2C::ICM_20948_I2C(job_period freq, I2C_HandleTypeDef* i2c_handle, bool alternative_address)
{
	this->i2c_handle = i2c_handle;
	jobRegister(freq);
	address = (alternative_address ? ICM20948_ADDRESS_2 : ICM20948_ADDRESS_1) << 1;
	init();
	return;
}

void ICM_20948_I2C::init()
{
	//The i2c bus might be stuck
	while (HAL_I2C_IsDeviceReady(i2c_handle, address, 20, ICM_20948_I2C_TIMEOUT) != HAL_OK) { ; }

	setBank(0);
	reset();
	disableSleep();
	setGyroRange(ICM20948_GYRO_RANGE_2000_DPS);
	setAccelRange(ICM20948_ACCEL_RANGE_16_G);

	setGyroRateDivider(10);
	setAccelRateDivider(10);

	HAL &hal = HAL::getInstance();
	StorageManager* sm = hal.getStorageManager();

	if (sm)
	{
		ConfigManager* cm = sm->getConfigManager();
		if(cm->getValue(ACCEL_BIAS_X_KEY, &accel_offset_x) != CONFIG_OK) accel_offset_x = 0.0;
		if(cm->getValue(ACCEL_BIAS_Y_KEY, &accel_offset_y) != CONFIG_OK) accel_offset_y = 0.0;
		if(cm->getValue(ACCEL_BIAS_Z_KEY, &accel_offset_z) != CONFIG_OK) accel_offset_z = 0.0;

		if(cm->getValue(GYRO_BIAS_X_KEY, &gyro_offset_x) != CONFIG_OK) gyro_offset_x = 0.0;
		if(cm->getValue(GYRO_BIAS_Y_KEY, &gyro_offset_y) != CONFIG_OK) gyro_offset_y = 0.0;
		if(cm->getValue(GYRO_BIAS_Z_KEY, &gyro_offset_z) != CONFIG_OK) gyro_offset_z = 0.0;

		if(cm->getValue(MAG_BIAS_X_KEY, &mag_offset_x) != CONFIG_OK) mag_offset_x = 0.0;
		if(cm->getValue(MAG_BIAS_Y_KEY, &mag_offset_y) != CONFIG_OK) mag_offset_y = 0.0;
		if(cm->getValue(MAG_BIAS_Z_KEY, &mag_offset_z) != CONFIG_OK) mag_offset_z = 0.0;
	}
	else
	{
		writeLog("Can not find config values for icm20948 driver");

		accel_offset_x = 0.0;
		accel_offset_y = 0.0;
		accel_offset_z = 0.0;

		gyro_offset_x = 0.0;
		gyro_offset_y = 0.0;
		gyro_offset_z = 0.0;

		mag_offset_x = 0.0;
		mag_offset_y = 0.0;
		mag_offset_z = 0.0;
	}

	setI2CBypass(false);
	configureI2CMaster();
	enableI2CMaster(true);
	if (!checkForMagConnection()) { faultHandler(); }
	setMagDataRate(AK09916_MAG_DATARATE_50_HZ);
	setUpMagProxy();

	writeLog("Initialized icm20948 driver");

	return;
}

void ICM_20948_I2C::setGyroRange(icm20948_gyro_range_t gyro_range)
{
	setBank(2);
	writeBit(ICM20X_B2_GYRO_CONFIG_1, 1, gyro_range & 0x01);
	writeBit(ICM20X_B2_GYRO_CONFIG_1, 2, gyro_range & 0x02);
	this->gyro_range = gyro_range;
	return;
}

void ICM_20948_I2C::setAccelRange(icm20948_accel_range_t accel_range)
{
	setBank(2);
	writeBit(ICM20X_B2_ACCEL_CONFIG_1, 1, accel_range & 0x01);
	writeBit(ICM20X_B2_ACCEL_CONFIG_1, 2, accel_range & 0x02);
	this->accel_range = accel_range;
	return;
}

uint8_t ICM_20948_I2C::readId()
{
	setBank(0);
	uint8_t id = readRegister(ICM20X_B0_WHOAMI);
	return id;
}

void ICM_20948_I2C::disableSleep()
{
	setBank(0);
	writeBit(ICM20X_B0_PWR_MGMT_1, 6, false);
	HAL_Delay(100);
	return;
}

void ICM_20948_I2C::reset()
{
	setBank(0);
	writeRegister(ICM20X_B0_PWR_MGMT_1, 1 << 7);
	HAL_Delay(100);
	selected_bank = -1;
	return;
}

void ICM_20948_I2C::setBank(uint8_t bank_number)
{
	if (selected_bank == (int8_t)bank_number){ return; }
	writeRegister(ICM20X_B0_REG_BANK_SEL, (bank_number << 4) & 0x30);
	selected_bank = bank_number;
	return;
}

void ICM_20948_I2C::setGyroRateDivider(uint16_t new_divider)
{
	setBank(2);
	writeRegister(ICM20X_B2_GYRO_SMPLRT_DIV, new_divider & 0xFF);
	return;
}

void ICM_20948_I2C::setAccelRateDivider(uint16_t new_divider)
{
	setBank(2);
	writeRegister(ICM20X_B2_ACCEL_SMPLRT_DIV_2, new_divider & 0xFF);
	writeBit(ICM20X_B2_ACCEL_SMPLRT_DIV_1, 0, new_divider & 256);
	writeBit(ICM20X_B2_ACCEL_SMPLRT_DIV_1, 1, new_divider & 512);
	writeBit(ICM20X_B2_ACCEL_SMPLRT_DIV_1, 2, new_divider & 1024);
	return;
}

void ICM_20948_I2C::jobLoop()
{
	setBank(0);
	uint8_t initial_register = ICM20X_B0_ACCEL_XOUT_H;
	uint8_t buffer[18];

	HAL_I2C_Master_Transmit(i2c_handle, address, &initial_register, 1, ICM_20948_I2C_TIMEOUT);
	return_code = HAL_I2C_Master_Receive(i2c_handle, address, buffer, 12, ICM_20948_I2C_TIMEOUT);
	if (return_code != HAL_OK) faultHandler();

	raw_accel_x = buffer[0] << 8 | buffer[1];
	raw_accel_y = buffer[2] << 8 | buffer[3];
	raw_accel_z = buffer[4] << 8 | buffer[5];

	float accel_scale = 1.0;

	if (accel_range == ICM20948_ACCEL_RANGE_2_G)
		accel_scale = 16384.0 / STANDARD_GRAVITY;
	if (accel_range == ICM20948_ACCEL_RANGE_4_G)
		accel_scale = 8192.0 / STANDARD_GRAVITY;
	if (accel_range == ICM20948_ACCEL_RANGE_8_G)
		accel_scale = 4096.0 / STANDARD_GRAVITY;
	if (accel_range == ICM20948_ACCEL_RANGE_16_G)
		accel_scale = 2048.0 / STANDARD_GRAVITY;

	accel_x = -(raw_accel_y / accel_scale) + accel_offset_x;
	accel_y = raw_accel_x / accel_scale + accel_offset_y;
	accel_z = raw_accel_z / accel_scale + accel_offset_z;

	raw_gyro_x = buffer[6] << 8 | buffer[7];
	raw_gyro_y = buffer[8] << 8 | buffer[9];
	raw_gyro_z = buffer[10] << 8 | buffer[11];

	float gyro_scale = 1.0;

	if (gyro_range == ICM20948_GYRO_RANGE_250_DPS)
		gyro_scale = 131.0;
	if (gyro_range == ICM20948_GYRO_RANGE_500_DPS)
		gyro_scale = 65.5;
	if (gyro_range == ICM20948_GYRO_RANGE_1000_DPS)
		gyro_scale = 32.8;
	if (gyro_range == ICM20948_GYRO_RANGE_2000_DPS)
		gyro_scale = 16.4;

	gyro_x = -(raw_gyro_y / gyro_scale) * (M_PI / 180) + gyro_offset_x;
	gyro_y = (raw_gyro_x / gyro_scale) * (M_PI / 180) + gyro_offset_y;
	gyro_z = (raw_gyro_z / gyro_scale) * (M_PI / 180) + gyro_offset_z;

	raw_mag_x = buffer[13] << 8 | buffer[12];
	raw_mag_y = buffer[15] << 8 | buffer[14];
	raw_mag_z = buffer[17] << 8 | buffer[16];

	//The axes of the magnetometer are inverted for some reason
	mag_x =   raw_mag_y * ICM20948_UT_PER_LSB  - mag_offset_x;
	mag_y =   raw_mag_x * ICM20948_UT_PER_LSB  - mag_offset_y;
	mag_z = -(raw_mag_z * ICM20948_UT_PER_LSB) - mag_offset_z;

	return;
}

void ICM_20948_I2C::accelCalibrate()
{
};

void ICM_20948_I2C::gyroCalibrate()
{
	float gyro_average_x = 0.0;
	float gyro_average_y = 0.0;
	float gyro_average_z = 0.0;

	uint16_t measurement = 0;

	while (measurement < 1000)
	{
		jobLoop();
		gyro_average_x += gyro_x;
		gyro_average_y += gyro_y;
		gyro_average_z += gyro_z;
		HAL_Delay(1);
		measurement++;
	}

	gyro_offset_x -= (gyro_average_x / 1000.0);
	gyro_offset_y -= (gyro_average_y / 1000.0);
	gyro_offset_z -= (gyro_average_z / 1000.0);

	writeLog("Calibrated gyroscope");
};

void ICM_20948_I2C::magCalibrate()
{
};


/*
 * This helper function allows to write only one bit of a certain register.
 * It performs a read operation and then a write operation, so it should only be used in non-critical routines.
 *
 */
void ICM_20948_I2C::writeBit(uint8_t register_number, uint8_t bit, bool set)
{
	uint8_t register_val;
	HAL_I2C_Master_Transmit(i2c_handle, address, &register_number, 1, ICM_20948_I2C_TIMEOUT);
	return_code = HAL_I2C_Master_Receive(i2c_handle, address, &register_val, 1, ICM_20948_I2C_TIMEOUT);
	if (return_code != HAL_OK) faultHandler();

	uint8_t buffer[2];
	buffer[0] = register_number;

	if (set){ buffer[1] =   (1 << bit)  | register_val; }
	else    { buffer[1] = (~(1 << bit)) & register_val; }

	return_code = HAL_I2C_Master_Transmit(i2c_handle, address, buffer, sizeof(buffer), ICM_20948_I2C_TIMEOUT);
	if (return_code != HAL_OK) faultHandler();

	return;
}

/*
 * This helper function writes a value to a register.
 *
 */

void ICM_20948_I2C::writeRegister(uint8_t register_number, uint8_t value)
{

	uint8_t buffer[2];
	buffer[0] = register_number;
	buffer[1] = value;
	return_code = HAL_I2C_Master_Transmit(i2c_handle, address, buffer, sizeof(buffer), ICM_20948_I2C_TIMEOUT);
	if (return_code != HAL_OK) faultHandler();
	return;
}

uint8_t ICM_20948_I2C::readRegister(uint8_t register_number)
{
	uint8_t return_val;
	HAL_I2C_Master_Transmit(i2c_handle, address, &register_number, 1, ICM_20948_I2C_TIMEOUT);
	return_code = HAL_I2C_Master_Receive(i2c_handle, address, &return_val, 1, ICM_20948_I2C_TIMEOUT);
	if (return_code != HAL_OK) faultHandler();
	return return_val;
}

void ICM_20948_I2C::setI2CBypass(bool state)
{
	setBank(0);
	writeBit(ICM20X_B0_REG_INT_PIN_CFG, 1, state);
}

void ICM_20948_I2C::configureI2CMaster()
{
	setBank(3);
	writeRegister(ICM20X_B3_I2C_MST_CTRL, 0x17);
}

void ICM_20948_I2C::enableI2CMaster(bool state)
{
	setBank(0);
	writeBit(ICM20X_B0_USER_CTRL, 5, state);
}

uint8_t ICM_20948_I2C::externalTransaction(uint8_t slave_address, uint8_t register_address, uint8_t value, bool read)
{
	setBank(3);

	uint8_t new_slave_address = slave_address;

	if (read)
	{
		new_slave_address |= 128;
	}
	else
	{
		writeRegister(ICM20X_B3_I2C_SLV4_DO, value);
	}

	writeRegister(ICM20X_B3_I2C_SLV4_ADDR, new_slave_address);
	writeRegister(ICM20X_B3_I2C_SLV4_REG, register_address);
	writeRegister(ICM20X_B3_I2C_SLV4_CTRL, 128);

	setBank(0);
	uint8_t tries = 0;
	// wait until the operation is finished
	while (!(readRegister(ICM20X_B0_I2C_MST_STATUS) & (1 << 6)))
	{
		tries++;
		if (tries >= NUM_FINISHED_CHECKS)
		{
			return (uint8_t) false;
		}
	}
	if (read)
	{
		setBank(3);
		return readRegister(ICM20X_B3_I2C_SLV4_DI);
	}
	return 1;
}

void ICM_20948_I2C::writeMagRegister(uint8_t register_address, uint8_t value)
{
	externalTransaction(0x0C, register_address, value, 0);
}

uint8_t ICM_20948_I2C::readMagRegister(uint8_t register_address)
{
	return externalTransaction(0x0C, register_address, 0, 1);
}

uint8_t ICM_20948_I2C::magReadId()
{
	uint8_t id = readMagRegister(0x01);
	return id;
}

void ICM_20948_I2C::resetI2CMaster()
{
	setBank(0);
	writeBit(ICM20X_B0_USER_CTRL, 1, true);
	while (true)
	{
		if (!(readRegister(ICM20X_B0_USER_CTRL) & 0x02)) { break; }
		HAL_Delay(10);
	}
	HAL_Delay(100);
	return;
}

bool ICM_20948_I2C::checkForMagConnection()
{
	bool aux_i2c_setup_failed = false;
	for (int i = 0; i < I2C_MASTER_RESETS_BEFORE_FAIL; i++)
	{
		if (magReadId() != ICM20948_MAG_ID)
		{
			resetI2CMaster();
		}
		else
		{
			aux_i2c_setup_failed = true;
			break;
		}
	}
	return aux_i2c_setup_failed;
}

void ICM_20948_I2C::setUpMagProxy()
{
	setBank(3);
	writeRegister(ICM20X_B3_I2C_SLV0_ADDR, 0x8C);
	writeRegister(ICM20X_B3_I2C_SLV0_REG,  0x10);
	writeRegister(ICM20X_B3_I2C_SLV0_CTRL, 0x89);
}

void ICM_20948_I2C::setMagDataRate(ak09916_data_rate_t data_rate)
{
	writeMagRegister(AK09916_CNTL2, AK09916_MAG_DATARATE_SHUTDOWN);
	HAL_Delay(1);
	writeMagRegister(AK09916_CNTL2, data_rate);
	return;
}

void ICM_20948_I2C::faultHandler()
{
	writeLog("Fault in ICM20948 driver");
	HAL::getInstance().softFaultHandler();
}



