/*
 * mode.cpp
 *
 *  Created on: Sep 21, 2021
 *      Author: alpemwarrior
 */

#include "interfaces/mode.hpp"
#include "hal.hpp"

Mode::Mode()
{
	out = HAL::getInstance().getOutputManager();
	rx = HAL::getInstance().getReceiver();
	mp = HAL::getInstance().getMotionProcessor();
}


