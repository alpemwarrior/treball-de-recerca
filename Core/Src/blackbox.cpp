/*
 * blackbox.cpp
 *
 *  Created on: 29 de set. 2021
 *      Author: alpemwarrior
 */

#include <stdio.h>

#include "blackbox.hpp"
#include "hal.hpp"

void Blackbox::jobLoop()
{
	if (accel == nullptr)
	{
		HAL &hal = HAL::getInstance();

		gyro = hal.getGyroscope();
		accel = hal.getAccelerometer();
		mag = hal.getMagnetometer();

		out = hal.getOutputManager();
		mp = hal.getMotionProcessor();
		return;
	}

	time = HAL_GetTick();

	/*

	bytes = snprintf((char*)buffer, sizeof(buffer), """{\"timestamp\":%lu, \"component\":\"gyroscope\", \"data\":[%f, %f, %f]}\n""",
	time, gyro->gyro_x,   gyro->gyro_y,   gyro->gyro_z);

	fres = f_write(file_handle, buffer, bytes, &rec_bytes);

	bytes = snprintf((char*)buffer, sizeof(buffer), """{\"timestamp\":%lu, \"component\":\"accelerometer\", \"data\":[%f, %f, %f]}\n""",
	time, accel->accel_x,   accel->accel_y,  accel->accel_z);

	fres = f_write(file_handle, buffer, bytes, &rec_bytes);

	bytes = snprintf((char*)buffer, sizeof(buffer), """{\"timestamp\":%lu, \"component\":\"magnetometer\", \"data\":[%f, %f, %f]}\n""",
	time, mag->mag_x,  mag->mag_y, mag->mag_z);

	fres = f_write(file_handle, buffer, bytes, &rec_bytes);

	*/

	bytes = snprintf((char*)buffer, sizeof(buffer), """{\"timestamp\":%lu, \"component\":\"complementary filter\", \"data\":[%f, %f, %f, %f]}\n""",
	time, mp->angular_position_quat.a,  mp->angular_position_quat.b, mp->angular_position_quat.c, mp->angular_position_quat.d);

	fres = f_write(file_handle, buffer, bytes, &rec_bytes);

	bytes = snprintf((char*)buffer, sizeof(buffer), """{\"timestamp\":%lu, \"component\":\"servo\", \"data\": [%u, %u, %u, %u]}\n""",
	time, out->get(0),  out->get(1), out->get(2), out->get(3));

	fres = f_write(file_handle, buffer, bytes, &rec_bytes);

	return;
}

void Blackbox::init(FIL* file_handle, job_period freq)
{
	this->file_handle = file_handle;

	accel = nullptr;

	jobRegister(freq);

	return;
}




