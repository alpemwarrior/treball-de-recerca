/*
 * debug_worker.cpp
 *
 *  Created on: 8 de set. 2021
 *      Author: alpemwarrior
 */

#include <stdio.h>
#include <cmath>

#include "utils/euler_angles.hpp"
#include "log_manager_wrapper.hpp"
#include "debug_worker.hpp"
#include "hal.hpp"

#define M_PI            3.14159265358979323846

DebugWorker::DebugWorker(UART_HandleTypeDef *handle, job_period frequency)
{
	this->handle = handle;
	jobRegister(frequency);
	lol = false;
	writeLog("Debug serial worker is active");
}

void DebugWorker::jobLoop()
{
	/* Debugging code */
	HAL &hal = HAL::getInstance();
	EulerAngles orientation = hal.getMotionProcessor()->angular_position_ea;
	//float quartenion_module = sqrt(pow(hal.getMotionProcessor()->angular_position.a, 2.0) + pow(hal.getMotionProcessor()->angular_position.b, 2.0) + pow(hal.getMotionProcessor()->angular_position.c, 2.0) + pow(hal.getMotionProcessor()->angular_position.d, 2.0));
	//bytes = snprintf(buffer, sizeof(buffer), "%fx%fx%fw%fwa%fab%fbc%fc\n", hal.getMotionProcessor()->var1, hal.getMotionProcessor()->var2, hal.getMotionProcessor()->var3,
	//		hal.getMotionProcessor()->angular_position.a / quartenion_module, hal.getMotionProcessor()->angular_position.b / quartenion_module,
	//		hal.getMotionProcessor()->angular_position.c / quartenion_module, hal.getMotionProcessor()->angular_position.d / quartenion_module);
	//hal.getMotionProcessor()->getAngularPositionDecomposed(&x, &y, &z);
	bytes = snprintf(buffer, sizeof(buffer), "y%fyp%fpr%fr\n", (orientation.z / (2 * M_PI)) * 360, (orientation.x / (2 * M_PI)) * 360, (orientation.y / (2 * M_PI)) * 360);
	printDebugString();
}

void DebugWorker::printDebugString()
{
	HAL_UART_Transmit(handle, (uint8_t*)buffer, bytes, 10);
}

/*
void DebugWorker::printDebugFloat(UART_HandleTypeDef* handle, float decimal_number)
{
	uint8_t buffer[64];
	int8_t size = snprintf((char*)buffer, 64, "%f", decimal_number);
	if (size < 0) {return;}
	buffer[size] = '\n';
    HAL_UART_Transmit(handle, buffer, size + 1, HAL_MAX_DELAY);
	return;
}
*/



