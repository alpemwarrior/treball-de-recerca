/*
 * servo_pwm.cpp
 *
 *  Created on: Jul 9, 2021
 *      Author: alpemwarrior
 */

#include "drivers/servo_pwm.hpp"
#include "log_manager_wrapper.hpp"

ServoPWM::ServoPWM(TIM_HandleTypeDef* timer, uint32_t channel, bool inverted)
{
	this->inverted = inverted;
	this->timer = timer;
	this->channel = channel;
	HAL_TIM_PWM_Start(timer, channel);
	return;
	writeLog("Created PWM servo");
}

void ServoPWM::update(uint16_t value)
{
	if (inverted)
	{
		if (value > HAL_SERVO_RANGE)
		{
			value = 0;
		}
		else
		{
			value = HAL_SERVO_RANGE - value;
		}
	}
	uint32_t arr = __HAL_TIM_GET_AUTORELOAD(timer);
	uint16_t out = (arr * value) / (HAL_SERVO_RANGE * (1000 / SERVO_PWM_RANGE)) + (arr * SERVO_PWM_MIN) / 1000;
	__HAL_TIM_SET_COMPARE(timer, channel, out);
	return;
}
