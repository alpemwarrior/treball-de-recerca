/*
 * task_manager.cpp
 *
 * Basic task manager. Like very basic
 *
 *  Created on: Sep 7, 2021
 *      Author: alpemwarrior
 */

#include "task_manager.hpp"
#include "log_manager_wrapper.hpp"
#include "hal.hpp"

void TaskManager::init()
{
	for (uint8_t i = 0; i < HAL_TASK_MANAGER_MAX_JOBS; i++)
	{
		job_list[i] = nullptr;
	}
	writeLog("Initialized task manager");
}

void TaskManager::registerTask(Job* job)
{
	for (uint8_t i = 0; i < HAL_TASK_MANAGER_MAX_JOBS; i++)
	{
		if (job_list[i] == nullptr)
		{
			job_list[i] = job;
			writeLog("Task registered");
			return;
		}
	}
	writeLog("Could not register task");
	return;
}

void TaskManager::loop()
{
	unsigned long time = HAL_GetTick();
	if (job_list[index])
	{
		if ((time - job_list[index]->last_execution) >= job_list[index]->frequency)
		{
			job_list[index]->jobLoop();
			job_list[index]->last_execution = time;
#if HAL_TASK_MANAGER_USE_LAST_EXECUTION_TIME
			job_list[index]->last_execution_time = HAL_GetTick() - time;
#endif
		}
	}
	else
	{
		index = 0;
		return;
	}
	index++;
	if (index >= sizeof(job_list)) index = 0;
	return;
}
